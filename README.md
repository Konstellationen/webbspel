# Ett webbspel från Konstellationen

Ett projekt påbörjat under Konstellationens första hackathon, i form av ett enklare webbläsarspel i p5.js.

## Design

Ett sidoscrollande spel där spelaren tar på sig rollen som en agent vars jobb det är att övervaka kommunikation. Detta görs genom att hen går fram till människor och scannar deras mobiler m.m. för att hitta olagligt material. Om möjligt slumpas vilken icke-spelarkaraktär som begår brott. Kan vara så att ingen begår brott alls.

## Starta spelet för testning

För att testa spelet, klona detta repo. Navigera sedan till `src`-mappen, och starta en test-webbserver:
```
cd src
python3 -m http.server
```
Öppna sedan en browser och gå till `localhost:8000`.

### <p xmlns:cc="http://creativecommons.org/ns#" >Art assets used in this work are licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0
<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 