// Author: Isak Andersson 2023
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <https://www.gnu.org/licenses/>.


// -------------------- TODO
// [ ] handle screen rotation on phone / remake / rescale canvas
// [x] set initial character position to one in scene specification
// [ ] scrolling when walking along edge of the screen
// [ ] drawing different images based on entity state (walk cycle etc)
// [x] portals to other scenes
// [ ] interaction dependent animations?

let is_debug = false;

const makeCounter = function(initial) {
    let n;
    if (initial) {
        n = initial;
    } else {
        n = 0;
    }
    return function() {
        let result = n;
        n++;
        return result;
    }
}

// enum CHARACTER_STATE {
const characterStateEnumerator = makeCounter();
const CHARACTER_STATE_IDLE = characterStateEnumerator();
const CHARACTER_STATE_WALKING_RIGHT = characterStateEnumerator();
const CHARACTER_STATE_WALKING_LEFT = characterStateEnumerator();
const CHARACTER_STATE_INTERACTING = characterStateEnumerator();
const CHARACTER_STATE_COUNT = characterStateEnumerator();
// }
const CHARACTER_STATES = [
    "idle",
    "walking right",
    "walking left",
    "interacting"
];

function printCharacterState() {
    console.log(CHARACTER_STATES[character.state]);
}

// default values, overridden by json file
let character = {
    width: 5,
    height: 20,
    x: 5,
    state: CHARACTER_STATE_IDLE
};

let game = {
    scene: {
        width: 500,
        default_starting_position: 100
    }
};

function enterScene(scene_name, portal_name) {
    console.log("entering scene '" + scene_name + "'");
    let s = game.scenes[scene_name];
    if (!s) {
        console.error("no scene named '" + scene_name + "'");
        return;
    }

    console.log(s);
    game.scene = s;
    let w = s.width;
    if (!w) {
        console.error("scene did not have a width set, setting default");
        w = 100;
    }
    let default_start = null;
    if (s.hasOwnProperty("default_starting_position")) {
        default_start = s.default_starting_position;
        if (default_start < 0 || default_start > w) {
            console.error("default start position out of bounds, resetting to middle");
            default_start = w / 2;
        }
    } else {
        default_start = w / 2;
    }

    character.x = default_start;
    if (portal_name) {
        let portal = s.portals[portal_name];
        if (portal) {
            character.x = portal.position;
            if (!(character.x) || character.x > w || character.x < 0) {
                console.error("something went wrong when setting character position through scene, resetting to middle");
                character.x = w / 2;
            }
        } else {
            console.log("no portal position retrieved from scene");
        }
    }
}

function loadFromJSON(json) {
    console.log("json finished loading loading data...");
    console.log("begin load from json...");

    console.log(json.character);
    character.width =  json.character.width;
    character.height = json.character.height;

    const setNamesToKeys = function(objs) {
        for (let name in objs) {
            objs[name].name = name;
        }
    }
    setNamesToKeys(json.scenes);
    setNamesToKeys(json.portals);
    for (let s in json.scenes) {
        setNamesToKeys(json.scenes[s].entities);
    }

    game.scenes = json.scenes;
    game.portals = json.portals;

    {
        let k = "is_debug";
        if (json.hasOwnProperty(k)) {
            is_debug = json[k];
        }
    }

    {
        let s_name = json.initial_scene;
        if (s_name) {
            enterScene(s_name, null);
        }
    }

    console.log("finished load from json!");
}

let game_json;
function preload() {
    console.log("begin preload...");
    let json_done = false;
    game_json = loadJSON("/data/game.json", loadFromJSON);
    console.log("finished preload...");
}
    

function setupCanvas() {
    // TODO: center the canvas, probably with CSS?

    // settings 
    const aspect_y = 10;
    const aspect_x = 30;
    
    const aspect = aspect_y / aspect_x;
    let h = windowWidth * aspect;
    let scale = 1.0;
    // handle downscaling just in case the screen height is less than
    // our desired height

    // toggle for testing
    const shouldAdjustScale = true;
    if (shouldAdjustScale && h > windowHeight) {
        scale = windowHeight / h;
    }
    const w = Math.floor(windowWidth * scale);
    h = Math.floor(h * scale);
    createCanvas(w, h);
}

function setup() {
    console.log("begin setup...");

    setupCanvas();

    console.log("finished setup!");
}

function portalInteract(payload) {
    let portal = game.portals[payload.name];
    if (!portal) {
        console.error("no portal named " + payload.name);
        return;
    }

    let scenes = portal.scenes;
    let one_scene = scenes.filter(s => s != game.scene.name);
    if (one_scene.length != 1) {
        console.error("could not figure out which scene to jump to");
        return;
    }
    let new_scene = one_scene[0];
    enterScene(new_scene, portal.name);
}

const interact_table = {
    "portal" : portalInteract
};

function compareInteractables(a, b) {
    let a_priori = a.interaction.priority || 0;
    let b_priori = b.interaction.priority || 0;
    return a_priori - b_priori;
}

function handleInteract(entities) {
    let sorted = entities.sort(compareInteractables);
    let target = sorted[0];
    let interaction = target.interaction;
    let handler = interact_table[interaction.type];
    if (handler == null) {
    } else {
        handler(interaction.payload);
    }
}


const SPACE = 32;
let left_held = false;
let right_held = false;
let space_pressed = false;
function update(scale) {
    let left_touched = false;
    let right_touched = false;
    let center_touched = false;

    {
        // update touch
        const center_width = 30 * scale;
        const half_center = center_width / 2;
        const half_width = width / 2;
        const left_max = half_width - half_center;
        const right_min = half_width + half_center;

        left_touched = touches.some(t => t.x < left_max);
        right_touched = touches.some(t => t.x > right_min);
        center_touched = touches.some(t => ((t.x > left_max) && (t.x < right_min))); 
    }

    {
        // update character position
        let direction = 0;
        const xspeed = 0.25;
        if (keyIsDown(LEFT_ARROW))  direction -= 1;
        if (keyIsDown(RIGHT_ARROW)) direction += 1;

        if (direction == 0) {
            if (left_touched)  direction -= 1;
            if (right_touched) direction += 1;
            // handle touch if not moving with arrows
        }

        character.x += direction * xspeed;

        if (character.state < CHARACTER_STATE_INTERACTING) {
            const map = [CHARACTER_STATE_WALKING_LEFT, CHARACTER_STATE_IDLE , CHARACTER_STATE_WALKING_RIGHT];
            character.state = map[direction + 1];
        }
    }

    let collided_entities = [];
    {
        // collect collided entities
        let es = game.scene.entities
        for (let ename in es) {
            let e = es[ename];
            let w = e.width;
            let hw = w / 2;
            let e_pos = e.position;

            let min = e_pos - hw;
            let max = e_pos + hw;

            hw = character.width / 2;
            let cright = character.x + hw;
            let cleft = character.x - hw;
            let right_collide = cright > min && !(cleft > max);
            let left_collide = cleft < max && !(cright < min);
            if (right_collide || left_collide) {
                let on_collision = e.on_collision;
                if (on_collision) {
                    console.log("NYI");
                } else {
                    collided_entities.push(e);
                }
            }
        }
    }

    if (center_touched || space_pressed) {
        let interactable = collided_entities.filter(e => e.hasOwnProperty("interaction"));
        if (interactable.length != 0) {
            handleInteract(interactable);
        }
    }

    space_pressed = false;
}

function draw() {
    background(255);

    let scale = width / 100;
    update(scale);

    {
        // draw entities
        let es = game.scene.entities;
        for (let ename in es) {
            let e = es[ename];
            let w = e.width;
            let x = e.position - (w / 2);
            w *= scale;
            x *= scale;
            let h = e.height * scale;
            let y = e.position_y * scale; // hmmm -_-
            let appearance = e.appearance;
            if (appearance) {
                if (appearance.type === "rectangle") {
                    let colour = appearance.color;
                    if (y == null || isNaN(y) || y == undefined) {
                        y = height - h;
                    }
                    if (colour) {
                        fill(colour[0], colour[1], colour[2]);
                    }
                    rect(x, y, w, h);
                }
            }
        }
    }

    {
        // draw character
        let w = character.width;
        let hw = w / 2;
        w *= scale;
        let x = (character.x - hw) * scale;
        let h = character.height * scale;
        let y = height - h;

        const colour = color(0,0,50);
        fill(colour);
        rect(x, y, w, h);
    }
}

function keyPressed() {
    if (!space_pressed && key == ' ') {
        space_pressed = true;
    }
}

function keyReleased() {
}
